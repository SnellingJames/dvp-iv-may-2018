//
//  Recipe.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/17/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import Foundation
import UIKit

class Recipe{
    
    //variables for saved and displayed recipes
    var name: String = ""
    var cookTime: String = ""
    var ingredients: String = ""
    var instructions: String = ""
    var attribution: String = ""
    var healthInfo: String = ""
    var recipePhotoURL: String = ""
    var sourceURL: String = ""
    
}
