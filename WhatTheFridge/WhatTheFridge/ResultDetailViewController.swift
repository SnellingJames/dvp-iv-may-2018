//
//  ResultDetailViewController.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/7/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import UIKit
import CoreData

class ResultDetailViewController: UIViewController {

    //outlets for image, title, and information
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTextBox: UITextView!
    
    //variables to get the data from the search results
    var detailedRecipe: Recipe = Recipe()
    
    var recipePhoto: UIImage = UIImage()
    
    //variables for presistence data
    private var appDelegate: AppDelegate!
    private var context: NSManagedObjectContext!
    private var entity: NSEntityDescription!
    private var newRecipe: NSManagedObject!

    override func viewDidLoad() {
        super.viewDidLoad()

        recipeNameLabel.text = detailedRecipe.name
        recipeTextBox.text = detailedRecipe.attribution + "\n\n" + detailedRecipe.cookTime + "\n\n" + detailedRecipe.ingredients + "\n\n" + detailedRecipe.instructions
        
        recipeImage.image = recipePhoto
        
        //variables for data persistence
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        entity = NSEntityDescription.entity(forEntityName: "SavedRecipes", in: context)
        newRecipe = NSManagedObject(entity: entity, insertInto: context)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //action to save recipe to core data
    @IBAction func saveRecipePressed(_ sender: Any) {
        
        newRecipe.setValue(detailedRecipe.name, forKey: "name")
        newRecipe.setValue(detailedRecipe.cookTime, forKey: "cookTime")
        newRecipe.setValue(detailedRecipe.recipePhotoURL, forKey: "photoURL")
        newRecipe.setValue(detailedRecipe.ingredients, forKey: "ingredients")
        newRecipe.setValue(detailedRecipe.instructions, forKey: "instructions")
        newRecipe.setValue(detailedRecipe.attribution, forKey: "credit")
        newRecipe.setValue(detailedRecipe.healthInfo, forKey: "healthInfo")
        newRecipe.setValue(detailedRecipe.sourceURL, forKey: "recipeURL")
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
        
        //triggering the tableView to reload when Save Recipe is pressed.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        
    }
    
    //action to display health info
    @IBAction func healthInfoPressed(_ sender: Any) {
        let title = "Health Information"
        
        let message = detailedRecipe.healthInfo
        
        let healthInfo = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        healthInfo.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(healthInfo, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
