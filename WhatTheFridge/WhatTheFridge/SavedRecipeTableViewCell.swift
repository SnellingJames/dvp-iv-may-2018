//
//  SavedRecipeTableViewCell.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/21/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import UIKit

class SavedRecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var recipeTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
