//
//  SavedTableViewController.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/21/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import UIKit
import CoreData

private var cellIdentifier = "cell"

class SavedTableViewController: UITableViewController {

    //variables for presistence data
    private var appDelegate: AppDelegate!
    private var context: NSManagedObjectContext!
    private var entity: NSEntityDescription!
    private var newRecipe: NSManagedObject!
    
    //array to hold the saved recipes
    var savedRecipes: [Recipe] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //so that the tableView will update automatically when a recipe is saved
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
        //variables for data persistence
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        entity = NSEntityDescription.entity(forEntityName: "SavedRecipes", in: context)
        newRecipe = NSManagedObject(entity: entity, insertInto: context)
        
        loadSavedRecipes()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return savedRecipes.count
    }

    //for reloading the tableView when new recipes are saved
    @objc func loadList(){
        //load data here
        DispatchQueue.main.async {
            self.loadSavedRecipes()
        }
        
    }
    
    //func to load saved Recipe data
    private func loadSavedRecipes(){
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SavedRecipes")
        
        do{
            //getting results of the fetch request
            let results:[NSManagedObject] = try context.fetch(fetchRequest)
            
            //creating an array of game objects with the retrieved values
            for obj in results{
                print(obj.description)
                
                //creating new array of saved recipes to fill cells on tableView
                let savedRecipe: Recipe = Recipe()
                
                if obj.value(forKey: "name") != nil{
                    savedRecipe.name = obj.value(forKey: "name") as! String
                }
                
                if obj.value(forKey: "credit") != nil{
                    savedRecipe.attribution = obj.value(forKey: "credit") as! String
                }
                
                if obj.value(forKey: "cookTime") != nil{
                    savedRecipe.cookTime = obj.value(forKey: "cookTime") as! String
                }
                
                if obj.value(forKey: "healthInfo") != nil{
                    savedRecipe.healthInfo = obj.value(forKey: "healthInfo") as! String
                }
                
                if obj.value(forKey: "ingredients") != nil{
                    savedRecipe.ingredients = obj.value(forKey: "ingredients") as! String
                }
                
                if obj.value(forKey: "instructions") != nil{
                    savedRecipe.instructions = obj.value(forKey: "instructions") as! String
                }
                
                if obj.value(forKey: "photoURL") != nil{
                    savedRecipe.recipePhotoURL = obj.value(forKey: "photoURL") as! String
                }
                
                if obj.value(forKey: "recipeURL") != nil{
                    savedRecipe.sourceURL = obj.value(forKey: "recipeURL") as! String
                }
                
                if savedRecipe.name != ""{
                    savedRecipes.append(savedRecipe)
                }
            }
            tableView.reloadData()
            //print(savedRecipes.count)
        }
        catch{
            assertionFailure()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SavedRecipeTableViewCell

        cell.recipeTitleLabel.text = savedRecipes[indexPath.row].name
        // Configure the cell...

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
     //MARK: - Navigation

    //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! SavedRecipeDetailViewController
        destination.detailedRecipe = savedRecipes[self.tableView.indexPathForSelectedRow!.row]
    }
    

}
