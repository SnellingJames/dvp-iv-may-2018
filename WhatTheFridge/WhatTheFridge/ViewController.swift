//
//  ViewController.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/7/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    //outlets for ingredients fields
    @IBOutlet weak var ingredient1: UITextField!
    @IBOutlet weak var ingredient2: UITextField!
    @IBOutlet weak var ingredient3: UITextField!
    @IBOutlet weak var ingredient4: UITextField!
    @IBOutlet weak var ingredient5: UITextField!
    @IBOutlet weak var ingredient6: UITextField!
    @IBOutlet weak var ingredient7: UITextField!
    @IBOutlet weak var ingredient8: UITextField!
    @IBOutlet weak var ingredient9: UITextField!
    @IBOutlet weak var ingredient10: UITextField!
    
    //outlets for diet and allergy controls views
    @IBOutlet weak var dietControlsView: UIView!
    @IBOutlet weak var allergyControlsView: UIView!
    
    //outlets for diet boxes and labels
    @IBOutlet weak var ketogenicBox: UIButton!
    @IBOutlet weak var paleoBox: UIButton!
    @IBOutlet weak var primalBox: UIButton!
    @IBOutlet weak var veganBox: UIButton!
    @IBOutlet weak var vegetarianBox: UIButton!
    @IBOutlet weak var whole30Box: UIButton!
    @IBOutlet weak var ketogenticLabel: UILabel!
    @IBOutlet weak var paleoLabel: UILabel!
    @IBOutlet weak var primalLabel: UILabel!
    @IBOutlet weak var veganLabel: UILabel!
    @IBOutlet weak var vegetarianLabel: UILabel!
    @IBOutlet weak var whole30Label: UILabel!
    
    //outlets for allergy boxes and labels
    @IBOutlet weak var dairyBox: UIButton!
    @IBOutlet weak var eggsBox: UIButton!
    @IBOutlet weak var glutenBox: UIButton!
    @IBOutlet weak var grainBox: UIButton!
    @IBOutlet weak var peanutBox: UIButton!
    @IBOutlet weak var seafoodBox: UIButton!
    @IBOutlet weak var sesameBox: UIButton!
    @IBOutlet weak var shellfishBox: UIButton!
    @IBOutlet weak var soyBox: UIButton!
    @IBOutlet weak var sulfitesBox: UIButton!
    @IBOutlet weak var treeNutsBox: UIButton!
    @IBOutlet weak var wheatBox: UIButton!
    @IBOutlet weak var dairyLabel: UILabel!
    @IBOutlet weak var eggsLabel: UILabel!
    @IBOutlet weak var glutenLabel: UILabel!
    @IBOutlet weak var grainLabel: UILabel!
    @IBOutlet weak var peanutLabel: UILabel!
    @IBOutlet weak var seafoodLabel: UILabel!
    @IBOutlet weak var sesameLabel: UILabel!
    @IBOutlet weak var shellfishLabel: UILabel!
    @IBOutlet weak var soyLabel: UILabel!
    @IBOutlet weak var sulfitesLabel: UILabel!
    @IBOutlet weak var treeNutsLabel: UILabel!
    @IBOutlet weak var wheatLabel: UILabel!
    
    //array to hold user input text fields
    var ingredientInput: [UITextField] = []
    
    //string to hold the final ingredients list for API submission
    var ingredientString: String = ""
    
    //arrays to hold diet selection choices
    var dietSelections: [UIButton] = []
    var dietLabels: [UILabel] = []
    
    //string to hold the final diet selections for API submission
    var dietString: String = ""
    
    //array to hold allergy selection choices
    var allergySelections: [UIButton] = []
    var allergyLabels: [UILabel] = []
    
    //string to hold the final allergy selections for API submission
    var allergyString: String = ""
    
    //variables to construct the complex API call
    var complexRequestURLBase: String = ""
    
    var complexRequestURLMiddle1: String = ""
    
    var complexRequestURLMiddle2: String = ""
    
    var complexRequestURLEnd: String = ""
    
    var complexRequestURLComplete: String = ""
    
    var dietPrefix: String = ""
    
    var allergyPrefix:String = ""
    
    //array to hold recipe objects for easy transfer to results page
    var recipeArray: [Recipe] = []

    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //adding user fields and buttons to arrays for creating API search strings
        loadingArraysUp()
        
        //setting text field delegates for keyboard return
        self.ingredient1.delegate = self
        self.ingredient2.delegate = self
        self.ingredient3.delegate = self
        self.ingredient4.delegate = self
        self.ingredient5.delegate = self
        self.ingredient6.delegate = self
        self.ingredient7.delegate = self
        self.ingredient8.delegate = self
        self.ingredient9.delegate = self
        self.ingredient10.delegate = self
        
        //saving the allergy and diet button defaults upon first run
        let firstRun = UserDefaults.standard.bool(forKey: "firstRun") as Bool
        if !firstRun{
            buttonDefaults()
            UserDefaults.standard.set(true, forKey: "firstRun")
        }
        
        //filling in the main part of the complex search URL for ingredients + diet or allergy
        //order goes base, ingredients, middle1, intolerances + allergyString (if applicable), middle2, diet + dietString (if applicable), end
        dietPrefix = "&diet="
        allergyPrefix = "&intolerances="
        complexRequestURLBase = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/searchComplex?mincopper=0&minSaturatedFat=0&maxfolicacid=10000&minvitaminb3=0&maxcholesterol=10000&maxCalories=10000&minmagnesium=0&includeIngredients="
        complexRequestURLMiddle1 = "&minvitamink=0&minvitaminb2=0&maxvitaminb2=10000"
        complexRequestURLMiddle2 = "&maxzinc=10000&maxCarbs=10000&mincholine=0&ranking=1&minAlcohol=0"
        complexRequestURLEnd = "&maxmanganese=10000&maxvitamine=10000&maxvitamind=10000&minzinc=0&minvitamine=0&maxcalcium=10000&minFiber=0&minCalories=150&maxSaturatedFat=10000&maxvitaminb3=10000&miniodine=0&maxAlcohol=10000&minProtein=5&miniron=0&maxiron=10000&minvitaminc=0&maxpotassium=10000&mincalcium=0&maxProtein=10000&minfolate=0&mincholesterol=0&minfolicacid=0&maxFiber=10000&minselenium=0&maxvitaminb1=10000&minfluoride=0&minvitaminb5=0&addRecipeInformation=true&minsodium=0&minCarbs=5&minSugar=0&minvitaminb12=0&maxcaffeine=10000&minmanganese=0&minphosphorus=0&mincaffeine=0&maxvitaminc=10000&maxvitaminb12=10000&maxselenium=10000&minvitaminb1=0&maxcopper=10000&instructionsRequired=true&maxfluoride=10000&maxvitaminb5=10000&maxVitaminA=10000&minFat=0&maxSugar=10000&minVitaminA=0&maxsodium=10000&offset=0&type=main+course&minvitaminb6=0&minpotassium=0&fillIngredients=true&maxphosphorus=10000&maxcholine=10000&maxvitamink=10000&maxvitaminb6=10000&maxmagnesium=10000&minvitamind=0&maxiodine=10000&maxfolate=10000&number=6&limitLicense=false&maxFat=10000"
        
        loadButtonDefaults()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func for keyboard dismiss upon pressing return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //action for special diets to be revealed
    @IBAction func dietPressed(_ sender: Any) {
        
        //resigning the keyboard if its still visible
        for item in ingredientInput{
            item.resignFirstResponder()
        }
        
        //checking if the controls are hidden or not
        if dietControlsView.isHidden==true{
            dietControlsView.isHidden = false
        } else{
            dietControlsView.isHidden = true
        }
    }
    
    //action for allergies to be revealed
    @IBAction func allergiesPressed(_ sender: Any) {
        
        //resigning the keyboard if its still visible
        for item in ingredientInput{
            item.resignFirstResponder()
        }
        
        //checking if the controls are hidden or not
        if allergyControlsView.isHidden == true{
            allergyControlsView.isHidden = false
        } else{
            allergyControlsView.isHidden = true
        }
    }
    
    //action for search
    @IBAction func searchPressed(_ sender: Any) {
        
        //clearing query strings each time search is pressed
        dietString = ""
        allergyString = ""
        ingredientString = ""
        
        //clear recipe results array each time search is pressed
        recipeArray.removeAll()
        
        //cycling through ingredients fields and adding them to a comma separated string of ingredients
        for item in ingredientInput{
            if item.text != ""{
                let replaced = item.text?.replacingOccurrences(of: " ", with: "+")
                ingredientString = ingredientString + replaced! + "%2C"
            }
            
            //resigning the keyboard if its still visible
            item.resignFirstResponder()
        }
        
        //building diet string for API call
        for item in dietSelections{
            if item.backgroundImage(for: .normal) == #imageLiteral(resourceName: "Checked"){
                dietString = dietString + dietLabels[item.tag].text!.lowercased() + "%2C"
            }
        }
        
        //building allergy string for API call
        for item in allergySelections{
            if item.backgroundImage(for: .normal) == #imageLiteral(resourceName: "Checked"){
                allergyString = allergyString + allergyLabels[item.tag].text!.lowercased() + "%2C"
            }
        }
        
        if dietString != ""{
            
            //remove final three characters for API query
            dietString.removeLast()
            dietString.removeLast()
            dietString.removeLast()
        }
        
        if allergyString != ""{
            
            //remove final three characters for API query
            allergyString.removeLast()
            allergyString.removeLast()
            allergyString.removeLast()
        }
        
        if ingredientString != ""{
            
            //remove final three characters for API query
            ingredientString.removeLast()
            ingredientString.removeLast()
            ingredientString.removeLast()
        }
       
        //building appropriate API search URLs based on user input
        if allergyString == "" && dietString == ""{
            
             //if no diet or allergy is selected, build the complex URL and perform the complex search
            complexRequestURLComplete = complexRequestURLBase + ingredientString + complexRequestURLMiddle1 + complexRequestURLMiddle2 + complexRequestURLEnd
            complexSearch()
            
        } else if allergyString == "" && dietString != ""{
            
            //if only diet is selected but no allergy, build the complex URL and perform the complex search
            complexRequestURLComplete = complexRequestURLBase + ingredientString + complexRequestURLMiddle1 + complexRequestURLMiddle2 + dietPrefix + dietString + complexRequestURLEnd
            complexSearch()
            
        } else if allergyString != "" && dietString == ""{
            
            //if only allergy is selected but no diet, build the complex URL and perform the complex search
            complexRequestURLComplete = complexRequestURLBase + ingredientString + complexRequestURLMiddle1 + allergyPrefix + allergyString + complexRequestURLMiddle2 + complexRequestURLEnd
            complexSearch()
        } else {
            
            //when both diet and allergy are selected, build the complex URL and perform the complex search
            complexRequestURLComplete = complexRequestURLBase + ingredientString + complexRequestURLMiddle1 + allergyPrefix + allergyString + complexRequestURLMiddle2 + dietPrefix + dietString + complexRequestURLEnd
            complexSearch()
        }

        //if all ingredient fields are empty, notify user
        if ingredientString == ""{
            let title = "Error"
            
            let message = "Please enter at least one ingredient to perform search"
            
            let errorAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(errorAlert, animated: true, completion: nil)
            
            return
        }
    }
    
    //func for performing search through Spoonacular API
    private func complexSearch(){
        
        //configuring data session
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        //getting the results of the search
        if let validURL1 = URL(string: complexRequestURLComplete){
            var request = URLRequest(url: validURL1)
            
            request.setValue("eS3z5HAntpmshGqS3IFLX8Rj98Vbp15HSYkjsnElm4cfIoQO3T" , forHTTPHeaderField: "X-Mashape-Key")
            request.setValue("spoonacular-recipe-food-nutrition-v1.p.mashape.com", forHTTPHeaderField: "X-Mashape-Host")
            
            request.httpMethod = "GET"
            
            let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                //print(data!)
                if let error = error{
                    print("Error downloading data: \(error)")
                    return
                }
                
                if let http = response as? HTTPURLResponse, let data = data{
                    if http.statusCode == 200{
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.parseJSON(data: data)
                        })
                    }
                }
            })
            task.resume()
        }
    }
    
    //func for parsing the returned data. this will be used to create recipe objects to pass to the results screen
    private func parseJSON(data: Data){
        //print(data)
        
        //variables to hold instructions and ingredients
        var recipeInstructions: String = ""
        var missedIngredients: String = ""
        var usedIngredients: String = ""
        
        do{
            let json = try JSON(data: data)
            
            for a in json["results"].arrayValue{
                //print(a)
                //getting the recipe name
                let name = a["title"].stringValue
                
                //getting the prep time
                let time = a["readyInMinutes"].stringValue + " minutes"
                
                //getting the applicable diets
                let diets = a["diets"].stringValue
                
                //getting weight watchers points
                let wW = a["weightWatcherSmartPoints"].stringValue
                
                //getting the health info
                let fat = a["fat"].stringValue
                let protein = a["protein"].stringValue
                let calories = a["calories"].stringValue
                let carbs = a["carbs"].stringValue
                
                //getting the name of the recipe creator
                let credit = a["creditsText"].stringValue
                
                //getting the URL for the recipe's image
                let image = a["image"].stringValue
                
                //getting the recipe's source URL
                let source = a["sourceUrl"].stringValue
                //getting the ingredients list
                for b in a["missedIngredients"].arrayValue{
                    missedIngredients = missedIngredients + b["originalString"].stringValue + "\n"
                }
                for e in a["usedIngredients"].arrayValue{
                    usedIngredients = usedIngredients + e["originalString"].stringValue + "\n"
                }
                
                //getting the recipe instructions
                for c in a["analyzedInstructions"].arrayValue{
                    for d in c["steps"].arrayValue{
                        recipeInstructions = recipeInstructions + d["number"].stringValue + ". " + d["step"].stringValue + "\r\n"
                    }
                }
                
                //creating a new recipe object based on the data retrieved
                let returnedRecipe = Recipe()
                returnedRecipe.name = name
                returnedRecipe.sourceURL = source
                returnedRecipe.instructions = recipeInstructions
                returnedRecipe.ingredients = usedIngredients + missedIngredients
                returnedRecipe.cookTime = "Prep Time: " + time
                returnedRecipe.healthInfo = "Diets: " + diets + "\n" + "Weight Watchers Smart Points: " + wW + "\n" + "Fat: " + fat + "\n" + "Protein: " + protein + "\n" + "Calories: " + calories + "\n" + "Carbs: " + carbs + "\n"
                returnedRecipe.attribution = "Recipe Credit: " + credit
                returnedRecipe.recipePhotoURL = image
                
                recipeArray.append(returnedRecipe)
                
                //clearing the variables declared outside the loops for the next recipe
                missedIngredients = ""
                usedIngredients = ""
                recipeInstructions = ""
            }
        }
        catch{
            return
        }
        performSegue(withIdentifier: "search", sender: AnyClass.self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ResultsViewController
        
        destination.resultRecipes = recipeArray
    }
    
    //action to check/uncheck diet boxes
    @IBAction func dietButtonPressed(_ sender: UIButton){
        
        //switch statement based on which button is pressed
        switch sender {
            
        case ketogenicBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if ketogenicBox.backgroundImage(for: .normal) == #imageLiteral(resourceName: "Unchecked"){
                ketogenicBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "ketogenic")
            }else{
                ketogenicBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "ketogenic")
            }
            
        case paleoBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if paleoBox.backgroundImage(for: .normal) == #imageLiteral(resourceName: "Unchecked"){
                paleoBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "paleo")
            }else{
                paleoBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "paleo")
            }
            
        case primalBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if primalBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                primalBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "primal")
            }else{
                primalBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "primal")
            }
            
        case veganBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if veganBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                veganBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "vegan")
            }else{
                veganBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "vegan")
            }
            
        case vegetarianBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if vegetarianBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                vegetarianBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "vegetarian")
            }else{
                vegetarianBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "vegetarian")
            }
            
        case whole30Box:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if whole30Box.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                whole30Box.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "whole30")
            }else{
                whole30Box.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "whole30")
            }
            
        default:
            return
        }
    }
    
    //action to check/uncheck allergy boxes
    @IBAction func allergySelectionPressed(_ sender: UIButton){
        
        //switch statement based on which button is pressed
        switch sender {
            
        case dairyBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if dairyBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                dairyBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "dairy")
            }else{
                dairyBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "dairy")
            }
            
        case eggsBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if eggsBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                eggsBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "eggs")
            }else{
                eggsBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "eggs")
            }
            
        case glutenBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if glutenBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                glutenBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "gluten")
            }else{
                glutenBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "gluten")
            }
            
        case grainBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if grainBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                grainBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "grain")
            }else{
                grainBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "grain")
            }
            
        case peanutBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if peanutBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                peanutBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "peanut")
            }else{
                peanutBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "peanut")
            }
            
        case seafoodBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if seafoodBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                seafoodBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "seafood")
            }else{
                seafoodBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "seafood")
            }
            
        case sesameBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if sesameBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                sesameBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "sesame")
            }else{
                sesameBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "sesame")
            }
            
        case shellfishBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if shellfishBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                shellfishBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "shellfish")
            }else{
                shellfishBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "shellfish")
            }
            
        case soyBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if soyBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                soyBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "soy")
            }else{
                soyBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "soy")
            }
            
        case sulfitesBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if sulfitesBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                sulfitesBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "sulfites")
            }else{
                sulfitesBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "sulfites")
            }
            
        case treeNutsBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if treeNutsBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                treeNutsBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "treeNuts")
            }else{
                treeNutsBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "treeNuts")
            }
            
        case wheatBox:
            
            //if button is unchecked, check it. if it is checked, uncheck it.
            if wheatBox.backgroundImage(for: .normal)==#imageLiteral(resourceName: "Unchecked"){
                wheatBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
                defaults.setValue(1, forKey: "wheat")
            }else{
                wheatBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
                defaults.setValue(0, forKey: "wheat")
            }
            
        default:
            return
        }
    }
    
    //setting button defaults to 0 for first run. 0 = unchecked
    private func buttonDefaults(){
        
        defaults.setValue(0, forKey: "ketogenic")
        defaults.setValue(0, forKey: "paleo")
        defaults.setValue(0, forKey: "primal")
        defaults.setValue(0, forKey: "vegan")
        defaults.setValue(0, forKey: "vegetarian")
        defaults.setValue(0, forKey: "whole30")
        
        defaults.setValue(0, forKey: "dairy")
        defaults.setValue(0, forKey: "eggs")
        defaults.setValue(0, forKey: "gluten")
        defaults.setValue(0, forKey: "grain")
        defaults.setValue(0, forKey: "peanut")
        defaults.setValue(0, forKey: "seafood")
        defaults.setValue(0, forKey: "sesame")
        defaults.setValue(0, forKey: "shellfish")
        defaults.setValue(0, forKey: "soy")
        defaults.setValue(0, forKey: "sulfites")
        defaults.setValue(0, forKey: "treeNuts")
        defaults.setValue(0, forKey: "wheat")
    }
    
    //loading user settings from defaults
    private func loadButtonDefaults(){
        
        let keto = defaults.integer(forKey: "ketogenic")
        let paleo = defaults.integer(forKey: "paleo")
        let primal = defaults.integer(forKey: "primal")
        let vegan = defaults.integer(forKey: "vegan")
        let vegetarian = defaults.integer(forKey: "vegetarian")
        let whole30 = defaults.integer(forKey: "whole30")
        let dairy = defaults.integer(forKey: "dairy")
        let eggs = defaults.integer(forKey: "eggs")
        let gluten = defaults.integer(forKey: "gluten")
        let grain = defaults.integer(forKey: "grain")
        let peanut = defaults.integer(forKey: "peanut")
        let seafood = defaults.integer(forKey: "seafood")
        let sesame = defaults.integer(forKey: "sesame")
        let shellfish = defaults.integer(forKey: "shellfish")
        let soy = defaults.integer(forKey: "soy")
        let sulfites = defaults.integer(forKey: "sulfites")
        let treeNuts = defaults.integer(forKey: "treeNuts")
        let wheat = defaults.integer(forKey: "wheat")
        
        //setting diet buttons based on returned Int. 0 = unchecked, 1 = checked
        if keto == 0{
            ketogenicBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            ketogenicBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if paleo == 0{
            paleoBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            paleoBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if primal == 0{
            primalBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            primalBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if vegan == 0{
            veganBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            veganBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if vegetarian == 0{
            vegetarianBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            vegetarianBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if whole30 == 0{
            whole30Box.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            whole30Box.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        
        //setting allergy buttons based on returned Int, 0 = unchecked, 1 = checked
        if dairy == 0{
            dairyBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            dairyBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if eggs == 0{
            eggsBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            eggsBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if gluten == 0{
            glutenBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            glutenBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if grain == 0{
            grainBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            grainBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if peanut == 0{
            peanutBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            peanutBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if seafood == 0{
            seafoodBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            seafoodBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if sesame == 0{
            sesameBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            sesameBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if shellfish == 0{
            shellfishBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            shellfishBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if soy == 0{
            soyBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            soyBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if sulfites == 0{
            sulfitesBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            sulfitesBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if treeNuts == 0{
            treeNutsBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            treeNutsBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
        if wheat == 0{
            wheatBox.setBackgroundImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
        } else {
            wheatBox.setBackgroundImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }
    }
    
    //func to load user selections and ingredients into arrays
    private func loadingArraysUp(){
        
        //adding text fields to an array for ease of API query
        ingredientInput.append(ingredient1)
        ingredientInput.append(ingredient2)
        ingredientInput.append(ingredient3)
        ingredientInput.append(ingredient4)
        ingredientInput.append(ingredient5)
        ingredientInput.append(ingredient6)
        ingredientInput.append(ingredient7)
        ingredientInput.append(ingredient8)
        ingredientInput.append(ingredient9)
        ingredientInput.append(ingredient10)
        
        //adding diet check boxes to an array for ease of API query
        dietSelections.append(ketogenicBox)
        dietSelections.append(primalBox)
        dietSelections.append(paleoBox)
        dietSelections.append(veganBox)
        dietSelections.append(vegetarianBox)
        dietSelections.append(whole30Box)
        
        //adding diet labels for cross referencing with checked boxes
        dietLabels.append(ketogenticLabel)
        dietLabels.append(primalLabel)
        dietLabels.append(paleoLabel)
        dietLabels.append(veganLabel)
        dietLabels.append(vegetarianLabel)
        dietLabels.append(whole30Label)
        
        //adding allergy check boxes to an array for ease of API query
        allergySelections.append(dairyBox)
        allergySelections.append(eggsBox)
        allergySelections.append(glutenBox)
        allergySelections.append(grainBox)
        allergySelections.append(peanutBox)
        allergySelections.append(seafoodBox)
        allergySelections.append(sesameBox)
        allergySelections.append(shellfishBox)
        allergySelections.append(soyBox)
        allergySelections.append(sulfitesBox)
        allergySelections.append(treeNutsBox)
        allergySelections.append(wheatBox)
        
        //adding diet labels for cross referncing with checked boxes
        allergyLabels.append(dairyLabel)
        allergyLabels.append(eggsLabel)
        allergyLabels.append(glutenLabel)
        allergyLabels.append(grainLabel)
        allergyLabels.append(peanutLabel)
        allergyLabels.append(seafoodLabel)
        allergyLabels.append(sesameLabel)
        allergyLabels.append(shellfishLabel)
        allergyLabels.append(soyLabel)
        allergyLabels.append(sulfitesLabel)
        allergyLabels.append(treeNutsLabel)
        allergyLabels.append(wheatLabel)
    }
}

