//
//  ResultsViewController.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/7/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {

    //outlets for images and labels
    @IBOutlet weak var buttonImage1: UIButton!
    @IBOutlet weak var buttonImage2: UIButton!
    @IBOutlet weak var buttonImage3: UIButton!
    @IBOutlet weak var buttonImage4: UIButton!
    @IBOutlet weak var buttonImage5: UIButton!
    @IBOutlet weak var buttonImage6: UIButton!
    @IBOutlet weak var resultLabel1: UILabel!
    @IBOutlet weak var resultLabel2: UILabel!
    @IBOutlet weak var resultLabel3: UILabel!
    @IBOutlet weak var resultLabel4: UILabel!
    @IBOutlet weak var resultLabel5: UILabel!
    @IBOutlet weak var resultLabel6: UILabel!
    
    //array to hold the result arrays
    var resultRecipes: [Recipe] = []
    
    //arrays to hold imageViews and labels
    var resultImageButtons: [UIButton] = []
    
    var resultLabels: [UILabel] = []
    
    //variable to hold recipe to send to details VC
    var sendRecipe: Recipe = Recipe()
    
    var sendPicture: UIImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //loading the result image array
        prepareResultImages()
        
        //loading the result label array
        prepareResultLabels()
        
        //cycling through result recipes and downloading their images
        for (index, element) in resultRecipes.enumerated(){
            
            let config = URLSessionConfiguration.default
            
            let session = URLSession(configuration: config)
            //print(element.recipePhotoURL)
            if let pictureURL = URL(string: element.recipePhotoURL){
                let task = session.dataTask(with: pictureURL , completionHandler: { (data, response, error) in
                    
                    if let error = error{
                        print("Error downloading data: \(error)")
                        return
                    }
                    
                    if let http = response as? HTTPURLResponse, let data = data{
                        if http.statusCode == 200{
                            let downloadedPic = UIImage(data: data)
                            
                            DispatchQueue.main.async(execute: {
                                self.resultImageButtons[index].setBackgroundImage(downloadedPic, for: .normal)
                                //self.resultImages.append(downloadedPic!)
                            })
                        }
                    }
                })
                task.resume()
            }
            //setting the labels to the recipe names
            resultLabels[index].text = element.name
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func recipeSelected(_ sender: UIButton){
        switch sender {
        case buttonImage1:
            sendRecipe = resultRecipes[0]
            sendPicture = sender.backgroundImage(for: .normal)!
            performSegue(withIdentifier: "goToDetail", sender: AnyClass.self)
            
        case buttonImage2:
            sendRecipe = resultRecipes[1]
            sendPicture = sender.backgroundImage(for: .normal)!
            performSegue(withIdentifier: "goToDetail", sender: AnyClass.self)
            
        case buttonImage3:
            sendRecipe = resultRecipes[2]
            sendPicture = sender.backgroundImage(for: .normal)!
            performSegue(withIdentifier: "goToDetail", sender: AnyClass.self)
            
        case buttonImage4:
            sendRecipe = resultRecipes[3]
            sendPicture = sender.backgroundImage(for: .normal)!
            performSegue(withIdentifier: "goToDetail", sender: AnyClass.self)
            
        case buttonImage5:
            sendRecipe = resultRecipes[4]
            sendPicture = sender.backgroundImage(for: .normal)!
            performSegue(withIdentifier: "goToDetail", sender: AnyClass.self)
            
        case buttonImage6:
            sendRecipe = resultRecipes[5]
            sendPicture = sender.backgroundImage(for: .normal)!
            performSegue(withIdentifier: "goToDetail", sender: AnyClass.self)
            
        default:
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ResultDetailViewController
        destination.detailedRecipe = sendRecipe
        destination.recipePhoto = sendPicture
    }
    
    //func to put labels in an array
    private func prepareResultLabels(){
        resultLabels.append(resultLabel1)
        resultLabels.append(resultLabel2)
        resultLabels.append(resultLabel3)
        resultLabels.append(resultLabel4)
        resultLabels.append(resultLabel5)
        resultLabels.append(resultLabel6)
    }
    
    //func to put buttons in an array
    private func prepareResultImages(){
        resultImageButtons.append(buttonImage1)
        resultImageButtons.append(buttonImage2)
        resultImageButtons.append(buttonImage3)
        resultImageButtons.append(buttonImage4)
        resultImageButtons.append(buttonImage5)
        resultImageButtons.append(buttonImage6)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
