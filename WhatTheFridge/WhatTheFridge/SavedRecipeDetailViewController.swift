//
//  SavedRecipeDetailViewController.swift
//  WhatTheFridge
//
//  Created by James Snelling on 5/10/18.
//  Copyright © 2018 James Snelling. All rights reserved.
//

import UIKit

class SavedRecipeDetailViewController: UIViewController {
    
    //outlets to display recipe
    @IBOutlet weak var savedImage: UIImageView!
    @IBOutlet weak var savedName: UILabel!
    @IBOutlet weak var savedTextView: UITextView!
    
    //variables to hold saved recipe
    var detailedRecipe: Recipe = Recipe()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //displaying saved recipe info
        savedName.text = detailedRecipe.name
        savedTextView.text = detailedRecipe.attribution + "\n\n" + detailedRecipe.cookTime + "\n\n" + detailedRecipe.ingredients + "\n\n" + detailedRecipe.instructions
        
        
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        //print(element.recipePhotoURL)
        if let pictureURL = URL(string: detailedRecipe.recipePhotoURL){
            let task = session.dataTask(with: pictureURL , completionHandler: { (data, response, error) in
                
                if let error = error{
                    print("Error downloading data: \(error)")
                    return
                }
                
                if let http = response as? HTTPURLResponse, let data = data{
                    if http.statusCode == 200{
                        let downloadedPic = UIImage(data: data)
                        
                        DispatchQueue.main.async(execute: {
                            self.savedImage.image = downloadedPic
                            //self.resultImages.append(downloadedPic!)
                        })
                    }
                }
            })
            task.resume()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func to display recipe health info
    @IBAction func healthInfoPressed(_ sender: Any) {
        let title = "Health Information"
        
        let message = detailedRecipe.healthInfo
        
        let healthInfo = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        healthInfo.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(healthInfo, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
